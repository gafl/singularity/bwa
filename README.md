# bwa Singularity container
### Bionformatics package bwa<br>
BWA is a software package for mapping DNA sequences against a large reference genome, such as the human genome. It consists of three algorithms: BWA-backtrack, BWA-SW and BWA-MEM. The first algorithm is designed for Illumina sequence reads up to 100bp, while the rest two for longer sequences ranged from 70bp to a few megabases<br>
bwa Version: 0.7.17<br>
[https://github.com/lh3/bwa]

Singularity container based on the recipe: Singularity.bwa_v0.7.17

Package installation using Miniconda3-4.7.12<br>

Image singularity (V>=3.3) is automatically build and deployed (gitlab-ci) and pushed on the registry using the .gitlab-ci.yml <br>

### build:
`sudo singularity build bwa_v0.7.17.sif Singularity.bwa_v0.7.17`

### Get image help
`singularity run-help ./bwa_v0.7.17.sif`

#### Default runscript: STAR
#### Usage:
  `bwa_v0.7.17.sif --help`<br>
    or:<br>
  `singularity exec bwa_v0.7.17.sif bwa --help`<br>


### Get image (singularity version >=3.3) with ORAS:<br>
`singularity pull bwa_v0.7.17.sif oras://registry.forgemia.inra.fr/gafl/singularity/bwa/bwa:latest`


